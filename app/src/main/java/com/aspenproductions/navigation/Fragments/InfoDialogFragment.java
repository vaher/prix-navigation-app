package com.aspenproductions.navigation.Fragments;

import androidx.fragment.app.DialogFragment;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

public class InfoDialogFragment extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder theDialog = new AlertDialog.Builder(getActivity());

        theDialog.setTitle("Väike info");

        String message = "Enne äpi käivitamist seadista kõik Brixid ära, siis kõik töötab.\r\n" +
                "Kui sa tunned, et arvutus on 'viltune', siis saab mängida kaoga.\r\n\r\n" +
                "Kui on vaja äpis muutusi teha, siis igaks juhuks mainin:\r\n" +
                "Värve, paigutust ja järjekordi on ülilihtne muuta;\r\n" +
                "Uusi funktsioone, lehekülgi, arvutuskäike on vaevarikas muuta/lisada.\r\n\r\n" +
                "Anna aga kindlasti teada, kui midagi vajab parendamist, isegi väiksed paigutused või värvid, " +
                "kõike saab 'timmida'.\r\n" +
                "Kas Mihkel pole mitte parim?";

        theDialog.setMessage(message);

        theDialog.setPositiveButton("Muidugi", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getActivity(), "Muidugi on Mihkel parim!", Toast.LENGTH_SHORT).show();
            }
        });

        theDialog.setNegativeButton("Jah", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getActivity(), "Jah Mihkel on parim!", Toast.LENGTH_SHORT).show();
            }
        });

        return theDialog.create();
    }
}
