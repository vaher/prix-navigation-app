package com.aspenproductions.navigation.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

public class NotChangeableSwitchFragment extends DialogFragment {


    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder theDialog = new AlertDialog.Builder(getActivity());

        theDialog.setTitle("Muuda nupp on väljas");

        theDialog.setMessage("Vigade vältimiseks peab laudade suuruse muutmiseks 'muuda' nupp sees olema!");

        theDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        return theDialog.create();
    }
}
