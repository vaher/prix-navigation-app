package com.aspenproductions.navigation.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

public class AddAndSubtractDialogFragment extends DialogFragment {


    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder theDialog = new AlertDialog.Builder(getActivity());

        theDialog.setTitle("Kaks tegevust korraga");

        theDialog.setMessage("Vigade vältimiseks korraga kas lisa või eemalda laudu!");

        theDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        return theDialog.create();
    }
}
