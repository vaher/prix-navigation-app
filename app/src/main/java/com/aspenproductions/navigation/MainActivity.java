package com.aspenproductions.navigation;

import com.aspenproductions.navigation.Arvutused.SecondScreen;
import com.aspenproductions.navigation.Fragments.EmptyDialogFragment;
import com.aspenproductions.navigation.Fragments.InfoDialogFragment;
import com.aspenproductions.navigation.Lauad.LaudFive;
import com.aspenproductions.navigation.Lauad.LaudFour;
import com.aspenproductions.navigation.Lauad.LaudOne;
import com.aspenproductions.navigation.Lauad.LaudSeven;
import com.aspenproductions.navigation.Lauad.LaudSix;
import com.aspenproductions.navigation.Lauad.LaudThree;
import com.aspenproductions.navigation.Lauad.LaudTwo;
import com.aspenproductions.navigation.Seadistus.MprixActivity;
import com.aspenproductions.navigation.Seadistus.RSprixActivity;
import com.aspenproductions.navigation.Seadistus.RprixActivity;
import com.aspenproductions.navigation.Seadistus.SprixActivity;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.EditText;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    EditText lauapikkus, laualaius, lauakorgus;

    double lauapik, laualai, lauakorg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Arvuta laua mõõtude järgi");

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            DialogFragment myFragment = new InfoDialogFragment();

            myFragment.show(getSupportFragmentManager(), "theDialog");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_rprix) {
            startActivity(new Intent(MainActivity.this, RprixActivity.class));

        } else if (id == R.id.nav_sprix) {
            startActivity(new Intent(MainActivity.this, SprixActivity.class));

        } else if (id == R.id.nav_mprix) {
            startActivity(new Intent(MainActivity.this, MprixActivity.class));

        } else if (id == R.id.nav_rsprix) {
            startActivity(new Intent(MainActivity.this, RSprixActivity.class));

        } else if (id == R.id.nav_laud_one) {
            startActivity(new Intent(MainActivity.this, LaudOne.class));

        } else if (id == R.id.nav_laud_two) {
            startActivity(new Intent(MainActivity.this, LaudTwo.class));

        } else if (id == R.id.nav_laud_three) {
            startActivity(new Intent(MainActivity.this, LaudThree.class));

        } else if (id == R.id.nav_laud_four) {
            startActivity(new Intent(MainActivity.this, LaudFour.class));

        } else if (id == R.id.nav_laud_five) {
            startActivity(new Intent(MainActivity.this, LaudFive.class));

        } else if (id == R.id.nav_laud_six) {
            startActivity(new Intent(MainActivity.this, LaudSix.class));

        } else if (id == R.id.nav_laud_seven) {
            startActivity(new Intent(MainActivity.this, LaudSeven.class));

        } else if (id == R.id.nav_total_laoseis) {
            startActivity(new Intent(MainActivity.this, TotalLaoseis.class));

        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onGenerateClick(View view) {

        Intent generate = new Intent(this, SecondScreen.class);

        lauapikkus = (EditText) findViewById(R.id.lauapikkus);
        laualaius = (EditText) findViewById(R.id.laualaius);
        lauakorgus = (EditText) findViewById(R.id.lauakorgus);

        String lauapikkusenumber = String.valueOf(lauapikkus.getText());
        String laualaiusenumber = String.valueOf(laualaius.getText());
        String lauakorgusenumber = String.valueOf(lauakorgus.getText());
        boolean filledlauapikkusenumber = !lauapikkusenumber.equals("");
        boolean filledlaualaiusenumber = !laualaiusenumber.equals("");
        boolean filledlauakorgusenumber = !lauakorgusenumber.equals("");

        if (filledlauapikkusenumber && filledlaualaiusenumber && filledlauakorgusenumber ) {
            lauapik = Double.parseDouble(lauapikkusenumber);
            laualai = Double.parseDouble(laualaiusenumber);
            lauakorg = Double.parseDouble(lauakorgusenumber);
            generate.putExtra("lauapikkus", lauapik);
            generate.putExtra("laualaius", laualai);
            generate.putExtra("lauakorgus", lauakorg);
            startActivity(generate);
        } else {
            errorMessage();
        }
    }

    private void errorMessage() {
        DialogFragment emptyFragment = new EmptyDialogFragment();
        emptyFragment.show(getSupportFragmentManager(), "theDialog");
    }
}
