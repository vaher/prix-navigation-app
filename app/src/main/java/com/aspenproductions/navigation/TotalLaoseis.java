package com.aspenproductions.navigation;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class TotalLaoseis  extends AppCompatActivity {

    TextView pikkusOne, laiusOne, korgusOne, laoseisOne, pikkusTwo, laiusTwo, korgusTwo, laoseisTwo,
    pikkusThree, laiusThree, korgusThree, laoseisThree, pikkusFour, laiusFour, korgusFour, laoseisFour,
    pikkusFive, laiusFive, korgusFive, laoseisFive, pikkusSix, laiusSix, korgusSix, laoseisSix,
    pikkusSeven, laiusSeven, korgusSeven, laoseisSeven;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.laoseis);

        pikkusOne = (TextView) findViewById(R.id.laoseispikkusone);
        laiusOne = (TextView) findViewById(R.id.laoseislaiusone);
        korgusOne = (TextView) findViewById(R.id.laoseiskorgusone);
        laoseisOne = (TextView) findViewById(R.id.laoseistotal_one);
        SharedPreferences laoLaudDetailsOne = this.getSharedPreferences("laoLaudDetailsOne", MODE_PRIVATE);
        pikkusOne.setText(String.valueOf(laoLaudDetailsOne.getString("pikkus_value", "")));
        laiusOne.setText(String.valueOf(laoLaudDetailsOne.getString("laius_value", "")));
        korgusOne.setText(String.valueOf(laoLaudDetailsOne.getString("korgus_value", "")));
        SharedPreferences laoseisDetailsOne = this.getSharedPreferences("laoseisOne", MODE_PRIVATE);
        laoseisOne.setText(String.valueOf(laoseisDetailsOne.getString("laoseis_value", "")));

        pikkusTwo = (TextView) findViewById(R.id.laoseispikkustwo);
        laiusTwo = (TextView) findViewById(R.id.laoseislaiustwo);
        korgusTwo = (TextView) findViewById(R.id.laoseiskorgustwo);
        laoseisTwo = (TextView) findViewById(R.id.laoseistotal_two);
        SharedPreferences laoLaudDetailsTwo = this.getSharedPreferences("laoLaudDetailsTwo", MODE_PRIVATE);
        pikkusTwo.setText(String.valueOf(laoLaudDetailsTwo.getString("pikkus_value", "")));
        laiusTwo.setText(String.valueOf(laoLaudDetailsTwo.getString("laius_value", "")));
        korgusTwo.setText(String.valueOf(laoLaudDetailsTwo.getString("korgus_value", "")));
        SharedPreferences laoseisDetailsTwo = this.getSharedPreferences("laoseisTwo", MODE_PRIVATE);
        laoseisTwo.setText(String.valueOf(laoseisDetailsTwo.getString("laoseis_value", "")));

        pikkusThree = (TextView) findViewById(R.id.laoseispikkusthree);
        laiusThree = (TextView) findViewById(R.id.laoseislaiusthree);
        korgusThree = (TextView) findViewById(R.id.laoseiskorgusthree);
        laoseisThree = (TextView) findViewById(R.id.laoseistotal_three);
        SharedPreferences laoLaudDetailsThree = this.getSharedPreferences("laoLaudDetailsThree", MODE_PRIVATE);
        pikkusThree.setText(String.valueOf(laoLaudDetailsThree.getString("pikkus_value", "")));
        laiusThree.setText(String.valueOf(laoLaudDetailsThree.getString("laius_value", "")));
        korgusThree.setText(String.valueOf(laoLaudDetailsThree.getString("korgus_value", "")));
        SharedPreferences laoseisDetailsThree = this.getSharedPreferences("laoseisThree", MODE_PRIVATE);
        laoseisThree.setText(String.valueOf(laoseisDetailsThree.getString("laoseis_value", "")));

        pikkusFour = (TextView) findViewById(R.id.laoseispikkusfour);
        laiusFour = (TextView) findViewById(R.id.laoseislaiusfour);
        korgusFour = (TextView) findViewById(R.id.laoseiskorgusfour);
        laoseisFour = (TextView) findViewById(R.id.laoseistotal_four);
        SharedPreferences laoLaudDetailsFour = this.getSharedPreferences("laoLaudDetailsFour", MODE_PRIVATE);
        pikkusFour.setText(String.valueOf(laoLaudDetailsFour.getString("pikkus_value", "")));
        laiusFour.setText(String.valueOf(laoLaudDetailsFour.getString("laius_value", "")));
        korgusFour.setText(String.valueOf(laoLaudDetailsFour.getString("korgus_value", "")));
        SharedPreferences laoseisDetailsFour = this.getSharedPreferences("laoseisFour", MODE_PRIVATE);
        laoseisFour.setText(String.valueOf(laoseisDetailsFour.getString("laoseis_value", "")));

        pikkusFive = (TextView) findViewById(R.id.laoseispikkusfive);
        laiusFive = (TextView) findViewById(R.id.laoseislaiusfive);
        korgusFive = (TextView) findViewById(R.id.laoseiskorgusfive);
        laoseisFive = (TextView) findViewById(R.id.laoseistotal_five);
        SharedPreferences laoLaudDetailsFive = this.getSharedPreferences("laoLaudDetailsFive", MODE_PRIVATE);
        pikkusFive.setText(String.valueOf(laoLaudDetailsFive.getString("pikkus_value", "")));
        laiusFive.setText(String.valueOf(laoLaudDetailsFive.getString("laius_value", "")));
        korgusFive.setText(String.valueOf(laoLaudDetailsFive.getString("korgus_value", "")));
        SharedPreferences laoseisDetailsFive = this.getSharedPreferences("laoseisFive", MODE_PRIVATE);
        laoseisFive.setText(String.valueOf(laoseisDetailsFive.getString("laoseis_value", "")));

        pikkusSix = (TextView) findViewById(R.id.laoseispikkussix);
        laiusSix = (TextView) findViewById(R.id.laoseislaiussix);
        korgusSix  = (TextView) findViewById(R.id.laoseiskorgussix);
        laoseisSix = (TextView) findViewById(R.id.laoseistotal_six);
        SharedPreferences laoLaudDetailsSix = this.getSharedPreferences("laoLaudDetailsSix", MODE_PRIVATE);
        pikkusSix.setText(String.valueOf(laoLaudDetailsSix.getString("pikkus_value", "")));
        laiusSix.setText(String.valueOf(laoLaudDetailsSix.getString("laius_value", "")));
        korgusSix.setText(String.valueOf(laoLaudDetailsSix.getString("korgus_value", "")));
        SharedPreferences laoseisDetailsSix = this.getSharedPreferences("laoseisSix", MODE_PRIVATE);
        laoseisSix.setText(String.valueOf(laoseisDetailsSix.getString("laoseis_value", "")));

        pikkusSeven = (TextView) findViewById(R.id.laoseispikkusseven);
        laiusSeven = (TextView) findViewById(R.id.laoseislaiusseven);
        korgusSeven = (TextView) findViewById(R.id.laoseiskorgusseven);
        laoseisSeven = (TextView) findViewById(R.id.laoseistotal_seven);
        SharedPreferences laoLaudDetailsSeven = this.getSharedPreferences("laoLaudDetailsSeven", MODE_PRIVATE);
        pikkusSeven.setText(String.valueOf(laoLaudDetailsSeven.getString("pikkus_value", "")));
        laiusSeven.setText(String.valueOf(laoLaudDetailsSeven.getString("laius_value", "")));
        korgusSeven.setText(String.valueOf(laoLaudDetailsSeven.getString("korgus_value", "")));
        SharedPreferences laoseisDetailsSeven = this.getSharedPreferences("laoseisSeven", MODE_PRIVATE);
        laoseisSeven.setText(String.valueOf(laoseisDetailsSeven.getString("laoseis_value", "")));
    }
}

