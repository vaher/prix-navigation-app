package com.aspenproductions.navigation.Lauad;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;

import com.aspenproductions.navigation.Fragments.AddAndSubtractDialogFragment;
import com.aspenproductions.navigation.Fragments.InfoDialogFragment;
import com.aspenproductions.navigation.Fragments.LaoSeisNotLessThanZeroFragment;
import com.aspenproductions.navigation.Fragments.NotChangeableSwitchFragment;
import com.aspenproductions.navigation.MainActivity;
import com.aspenproductions.navigation.Seadistus.MprixActivity;
import com.aspenproductions.navigation.R;
import com.aspenproductions.navigation.Seadistus.RSprixActivity;
import com.aspenproductions.navigation.Seadistus.RprixActivity;
import com.aspenproductions.navigation.Seadistus.SprixActivity;
import com.aspenproductions.navigation.TotalLaoseis;
import com.google.android.material.navigation.NavigationView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LaudTwo extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    SharedPreferences sharedpreference;

    EditText pikkus, laius, korgus, lisa, eemalda;
    TextView laoseis, lisamiseaeg, eemaldamiseaeg;
    Switch muuda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.laud_one);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Laud nr 2 laoseis");

        sharedpreference = PreferenceManager.getDefaultSharedPreferences(this.getBaseContext());

        pikkus = (EditText) findViewById(R.id.pikkus);
        laius = (EditText) findViewById(R.id.laius);
        korgus = (EditText) findViewById(R.id.korgus);
        SharedPreferences laoLaudDetails = this.getSharedPreferences("laoLaudDetailsTwo", MODE_PRIVATE);
        pikkus.setText(String.valueOf(laoLaudDetails.getString("pikkus_value", "")));
        laius.setText(String.valueOf(laoLaudDetails.getString("laius_value", "")));
        korgus.setText(String.valueOf(laoLaudDetails.getString("korgus_value", "")));

        laoseis = (TextView)findViewById(R.id.laoseis);
        lisa = (EditText) findViewById(R.id.lisa);
        eemalda = (EditText) findViewById(R.id.eemalda);
        SharedPreferences laoseisDetails = this.getSharedPreferences("laoseisTwo", MODE_PRIVATE);
        laoseis.setText(String.valueOf(laoseisDetails.getString("laoseis_value", "")));

        SharedPreferences timeDetails = this.getSharedPreferences("lastchangeTwo", MODE_PRIVATE);
        lisamiseaeg = (TextView)findViewById(R.id.lisamiseaeg);
        lisamiseaeg.setText(String.valueOf(timeDetails.getString("lastadd_value", "")));
        eemaldamiseaeg = (TextView)findViewById(R.id.eemaldamiseaeg);
        eemaldamiseaeg.setText(String.valueOf(timeDetails.getString("lastelim_value", "")));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void onSalvestaLadu(View view) {
        muuda = (Switch) findViewById(R.id.muudanupp);
        if(muuda.isChecked()) {
            SharedPreferences laoLaudDetails = this.getSharedPreferences("laoLaudDetailsTwo", MODE_PRIVATE);
            SharedPreferences.Editor edit = laoLaudDetails.edit();
            edit.putString("pikkus_value", pikkus.getText().toString());
            edit.putString("laius_value", laius.getText().toString());
            edit.putString("korgus_value", korgus.getText().toString());
            edit.apply();
            Toast.makeText(this, "Laua mõõtmed salvestatud", Toast.LENGTH_SHORT).show();
        }
        else {
            DialogFragment errorMessage = new NotChangeableSwitchFragment();
            errorMessage.show(getSupportFragmentManager(), "theDialog");
        }
    }

    public void onUuendaLaoseis(View view) {
        SharedPreferences laoLaudDetails = this.getSharedPreferences("laoseisTwo", MODE_PRIVATE);
        SharedPreferences.Editor edit = laoLaudDetails.edit();

        String currentLaoseis = String.valueOf(laoseis.getText());
        String addToLaoseis = String.valueOf(lisa.getText());
        String subtractFromLaoseis = String.valueOf(eemalda.getText());
        boolean filledCurrentLaoseis = !currentLaoseis.equals("");
        boolean filledAddToLaoseis = !addToLaoseis.equals("");
        boolean filledsubtractFromLaoseis = !subtractFromLaoseis.equals("");

        if (filledAddToLaoseis && filledsubtractFromLaoseis){
            DialogFragment errorMessage = new AddAndSubtractDialogFragment();
            errorMessage.show(getSupportFragmentManager(), "theDialog");
            return;
        }

        SharedPreferences timeDetails = this.getSharedPreferences("lastchangeTwo", MODE_PRIVATE);
        SharedPreferences.Editor time = timeDetails.edit();

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy    HH:mm:ss");
        String dateString = formatter.format(new Date(System.currentTimeMillis()));


        int hetkeLaoseis;
        if (filledCurrentLaoseis) {
            hetkeLaoseis = Integer.parseInt(currentLaoseis);
        } else {
            hetkeLaoseis = 0;
        }
        int lisaLaoseisule;
        if (filledAddToLaoseis) {
            lisaLaoseisule = Integer.parseInt(addToLaoseis);
            time.putString("lastadd_value", String.format("Viimane lisamine (%s):  %s", addToLaoseis, dateString));
        } else {
            lisaLaoseisule = 0;
        }
        int eemaldaLaoseisult;
        if (filledsubtractFromLaoseis) {
            eemaldaLaoseisult = Integer.parseInt(subtractFromLaoseis);
            time.putString("lastelim_value", String.format("Viimane eemaldamine (%s):  %s", subtractFromLaoseis, dateString));
        } else {
            eemaldaLaoseisult = 0;
        }
        int newLaoseis = hetkeLaoseis + lisaLaoseisule - eemaldaLaoseisult;
        if (newLaoseis < 0) {
            DialogFragment errorMessage = new LaoSeisNotLessThanZeroFragment();
            errorMessage.show(getSupportFragmentManager(), "theDialog");
            return;
        }
        edit.putString("laoseis_value", String.valueOf(newLaoseis));
        edit.apply();

        time.apply();

        Toast.makeText(this, String.format("Laoseis uuendatud, uus kogus %d",newLaoseis),
                Toast.LENGTH_LONG).show();

        Intent refresh = new Intent(this, LaudTwo.class);
        startActivity(refresh);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            DialogFragment myFragment = new InfoDialogFragment();

            myFragment.show(getSupportFragmentManager(), "theDialog");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(LaudTwo.this, MainActivity.class));
        } else if (id == R.id.nav_rprix) {
            startActivity(new Intent(LaudTwo.this, RprixActivity.class));

        } else if (id == R.id.nav_sprix) {
            startActivity(new Intent(LaudTwo.this, SprixActivity.class));

        } else if (id == R.id.nav_mprix) {
            startActivity(new Intent(LaudTwo.this, MprixActivity.class));
        } else if (id == R.id.nav_rsprix) {
            startActivity(new Intent(LaudTwo.this, RSprixActivity.class));

        } else if (id == R.id.nav_laud_one) {
            startActivity(new Intent(LaudTwo.this, LaudOne.class));

        } else if (id == R.id.nav_laud_two) {

        } else if (id == R.id.nav_laud_three) {
            startActivity(new Intent(LaudTwo.this, LaudThree.class));

        } else if (id == R.id.nav_laud_four) {
            startActivity(new Intent(LaudTwo.this, LaudFour.class));

        } else if (id == R.id.nav_laud_five) {
            startActivity(new Intent(LaudTwo.this, LaudFive.class));

        } else if (id == R.id.nav_laud_six) {
            startActivity(new Intent(LaudTwo.this, LaudSix.class));

        } else if (id == R.id.nav_laud_seven) {
            startActivity(new Intent(LaudTwo.this, LaudSeven.class));

        } else if (id == R.id.nav_total_laoseis) {
            startActivity(new Intent(LaudTwo.this, TotalLaoseis.class));

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
