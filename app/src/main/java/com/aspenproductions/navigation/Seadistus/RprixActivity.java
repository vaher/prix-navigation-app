package com.aspenproductions.navigation.Seadistus;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.aspenproductions.navigation.Fragments.InfoDialogFragment;
import com.aspenproductions.navigation.Lauad.LaudFive;
import com.aspenproductions.navigation.Lauad.LaudFour;
import com.aspenproductions.navigation.Lauad.LaudOne;
import com.aspenproductions.navigation.Lauad.LaudSeven;
import com.aspenproductions.navigation.Lauad.LaudSix;
import com.aspenproductions.navigation.Lauad.LaudThree;
import com.aspenproductions.navigation.Lauad.LaudTwo;
import com.aspenproductions.navigation.MainActivity;
import com.aspenproductions.navigation.R;
import com.aspenproductions.navigation.TotalLaoseis;
import com.google.android.material.navigation.NavigationView;

public class RprixActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    EditText rpikkus, rlaius, rkorgus, rkadu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rprix);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("R-Prix");

        rpikkus = (EditText) findViewById(R.id.rpikkus);
        rlaius = (EditText) findViewById(R.id.rlaius);
        rkorgus = (EditText) findViewById(R.id.rkorgus);
        rkadu = (EditText) findViewById(R.id.rkadu);

        SharedPreferences rprixDetails = this.getSharedPreferences("rprixdetails", MODE_PRIVATE);

        rpikkus.setText(String.valueOf(rprixDetails.getString("rpikkus_value", "")));
        rlaius.setText(String.valueOf(rprixDetails.getString("rlaius_value", "")));
        rkorgus.setText(String.valueOf(rprixDetails.getString("rkorgus_value", "")));
        rkadu.setText(String.valueOf(rprixDetails.getString("rkadu_value", "")));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void onRSaveClick (View view){
        SharedPreferences rprixDetails = this.getSharedPreferences("rprixdetails", MODE_PRIVATE);
        SharedPreferences.Editor edit = rprixDetails.edit();
        edit.putString("rpikkus_value", rpikkus.getText().toString());
        edit.putString("rlaius_value", rlaius.getText().toString());
        edit.putString("rkorgus_value", rkorgus.getText().toString());
        edit.putString("rkadu_value", rkadu.getText().toString());
        edit.apply();

        Intent home = new Intent(this, MainActivity.class);
        Toast.makeText(this, "R-Prixi mõõtmed salvestatud", Toast.LENGTH_SHORT).show();
        startActivity(home);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            DialogFragment myFragment = new InfoDialogFragment();

            myFragment.show(getSupportFragmentManager(), "theDialog");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(RprixActivity.this, MainActivity.class));
        } else if (id == R.id.nav_rprix) {

        } else if (id == R.id.nav_sprix) {
            startActivity(new Intent(RprixActivity.this, SprixActivity.class));

        } else if (id == R.id.nav_mprix) {
            startActivity(new Intent(RprixActivity.this, MprixActivity.class));

        } else if (id == R.id.nav_rsprix) {
            startActivity(new Intent(RprixActivity.this, RSprixActivity.class));

        } else if (id == R.id.nav_laud_one) {
            startActivity(new Intent(RprixActivity.this, LaudOne.class));

        } else if (id == R.id.nav_laud_two) {
            startActivity(new Intent(RprixActivity.this, LaudTwo.class));

        } else if (id == R.id.nav_laud_three) {
            startActivity(new Intent(RprixActivity.this, LaudThree.class));

        } else if (id == R.id.nav_laud_four) {
            startActivity(new Intent(RprixActivity.this, LaudFour.class));

        } else if (id == R.id.nav_laud_five) {
            startActivity(new Intent(RprixActivity.this, LaudFive.class));

        } else if (id == R.id.nav_laud_six) {
            startActivity(new Intent(RprixActivity.this, LaudSix.class));

        } else if (id == R.id.nav_laud_seven) {
            startActivity(new Intent(RprixActivity.this, LaudSeven.class));

        } else if (id == R.id.nav_total_laoseis) {
            startActivity(new Intent(RprixActivity.this, TotalLaoseis.class));

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
