package com.aspenproductions.navigation.Seadistus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;

import com.aspenproductions.navigation.Fragments.InfoDialogFragment;
import com.aspenproductions.navigation.Lauad.LaudFive;
import com.aspenproductions.navigation.Lauad.LaudFour;
import com.aspenproductions.navigation.Lauad.LaudOne;
import com.aspenproductions.navigation.Lauad.LaudSeven;
import com.aspenproductions.navigation.Lauad.LaudSix;
import com.aspenproductions.navigation.Lauad.LaudThree;
import com.aspenproductions.navigation.Lauad.LaudTwo;
import com.aspenproductions.navigation.MainActivity;
import com.aspenproductions.navigation.R;
import com.aspenproductions.navigation.TotalLaoseis;
import com.google.android.material.navigation.NavigationView;

public class RSprixActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    EditText rspikkus, rslaius, rskorgus, rskadu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rsprix);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("RS-Prix");

        rspikkus = (EditText) findViewById(R.id.rspikkus);
        rslaius = (EditText) findViewById(R.id.rslaius);
        rskorgus = (EditText) findViewById(R.id.rskorgus);
        rskadu = (EditText) findViewById(R.id.rskadu);

        SharedPreferences rsprixDetails = this.getSharedPreferences("rsprixdetails", MODE_PRIVATE);

        rspikkus.setText(String.valueOf(rsprixDetails.getString("rspikkus_value", "")));
        rslaius.setText(String.valueOf(rsprixDetails.getString("rslaius_value", "")));
        rskorgus.setText(String.valueOf(rsprixDetails.getString("rskorgus_value", "")));
        rskadu.setText(String.valueOf(rsprixDetails.getString("rskadu_value", "")));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void onRSSaveClick (View view){
        SharedPreferences rsprixDetails = this.getSharedPreferences("rsprixdetails", MODE_PRIVATE);
        SharedPreferences.Editor edit = rsprixDetails.edit();
        edit.putString("rspikkus_value", rspikkus.getText().toString());
        edit.putString("rslaius_value", rslaius.getText().toString());
        edit.putString("rskorgus_value", rskorgus.getText().toString());
        edit.putString("rskadu_value", rskadu.getText().toString());
        edit.apply();

        Intent home = new Intent(this, MainActivity.class);
        Toast.makeText(this, "RS-Prixi mõõtmed salvestatud", Toast.LENGTH_SHORT).show();
        startActivity(home);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            DialogFragment myFragment = new InfoDialogFragment();

            myFragment.show(getSupportFragmentManager(), "theDialog");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(RSprixActivity.this, MainActivity.class));
        } else if (id == R.id.nav_rprix) {
            startActivity(new Intent(RSprixActivity.this, RprixActivity.class));

        } else if (id == R.id.nav_sprix) {
            startActivity(new Intent(RSprixActivity.this, SprixActivity.class));

        } else if (id == R.id.nav_mprix) {
            startActivity(new Intent(RSprixActivity.this, MprixActivity.class));

        } else if (id == R.id.nav_rsprix) {

        } else if (id == R.id.nav_laud_one) {
            startActivity(new Intent(RSprixActivity.this, LaudOne.class));

        } else if (id == R.id.nav_laud_two) {
            startActivity(new Intent(RSprixActivity.this, LaudTwo.class));

        } else if (id == R.id.nav_laud_three) {
            startActivity(new Intent(RSprixActivity.this, LaudThree.class));

        } else if (id == R.id.nav_laud_four) {
            startActivity(new Intent(RSprixActivity.this, LaudFour.class));

        }
        else if (id == R.id.nav_laud_five) {
            startActivity(new Intent(RSprixActivity.this, LaudFive.class));

        } else if (id == R.id.nav_laud_six) {
            startActivity(new Intent(RSprixActivity.this, LaudSix.class));

        } else if (id == R.id.nav_laud_seven) {
            startActivity(new Intent(RSprixActivity.this, LaudSeven.class));

        } else if (id == R.id.nav_total_laoseis) {
            startActivity(new Intent(RSprixActivity.this, TotalLaoseis.class));

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
