package com.aspenproductions.navigation.Seadistus;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.aspenproductions.navigation.Fragments.InfoDialogFragment;
import com.aspenproductions.navigation.Lauad.LaudFive;
import com.aspenproductions.navigation.Lauad.LaudFour;
import com.aspenproductions.navigation.Lauad.LaudOne;
import com.aspenproductions.navigation.Lauad.LaudSeven;
import com.aspenproductions.navigation.Lauad.LaudSix;
import com.aspenproductions.navigation.Lauad.LaudThree;
import com.aspenproductions.navigation.Lauad.LaudTwo;
import com.aspenproductions.navigation.MainActivity;
import com.aspenproductions.navigation.R;
import com.aspenproductions.navigation.TotalLaoseis;
import com.google.android.material.navigation.NavigationView;

public class MprixActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    EditText mpikkus, mlaius, mkorgus, mkadu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mprix);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("M-Prix");

        mpikkus = (EditText) findViewById(R.id.mpikkus);
        mlaius = (EditText) findViewById(R.id.mlaius);
        mkorgus = (EditText) findViewById(R.id.mkorgus);
        mkadu = (EditText) findViewById(R.id.mkadu);

        SharedPreferences mprixDetails = this.getSharedPreferences("mprixdetails", MODE_PRIVATE);

        mpikkus.setText(String.valueOf(mprixDetails.getString("mpikkus_value", "")));
        mlaius.setText(String.valueOf(mprixDetails.getString("mlaius_value", "")));
        mkorgus.setText(String.valueOf(mprixDetails.getString("mkorgus_value", "")));
        mkadu.setText(String.valueOf(mprixDetails.getString("mkadu_value", "")));


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

        public void onMSaveClick (View view){
            SharedPreferences mprixDetails = this.getSharedPreferences("mprixdetails", MODE_PRIVATE);
            SharedPreferences.Editor edit = mprixDetails.edit();
            edit.putString("mpikkus_value", mpikkus.getText().toString());
            edit.putString("mlaius_value", mlaius.getText().toString());
            edit.putString("mkorgus_value", mkorgus.getText().toString());
            edit.putString("mkadu_value", mkadu.getText().toString());
            edit.apply();

            Intent home = new Intent(this, MainActivity.class);
            Toast.makeText(this, "M-Prixi mõõtmed salvestatud", Toast.LENGTH_SHORT).show();
            startActivity(home);
        }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            DialogFragment myFragment = new InfoDialogFragment();

            myFragment.show(getSupportFragmentManager(), "theDialog");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(MprixActivity.this, MainActivity.class));
        } else if (id == R.id.nav_rprix) {
            startActivity(new Intent(MprixActivity.this, RprixActivity.class));

        } else if (id == R.id.nav_sprix) {
            startActivity(new Intent(MprixActivity.this, SprixActivity.class));

        } else if (id == R.id.nav_mprix) {

        } else if (id == R.id.nav_rsprix) {
            startActivity(new Intent(MprixActivity.this, RSprixActivity.class));

        } else if (id == R.id.nav_laud_one) {
            startActivity(new Intent(MprixActivity.this, LaudOne.class));

        } else if (id == R.id.nav_laud_two) {
            startActivity(new Intent(MprixActivity.this, LaudTwo.class));

        } else if (id == R.id.nav_laud_three) {
            startActivity(new Intent(MprixActivity.this, LaudThree.class));

        } else if (id == R.id.nav_laud_four) {
            startActivity(new Intent(MprixActivity.this, LaudFour.class));

        } else if (id == R.id.nav_laud_five) {
            startActivity(new Intent(MprixActivity.this, LaudFive.class));

        } else if (id == R.id.nav_laud_six) {
            startActivity(new Intent(MprixActivity.this, LaudSix.class));

        } else if (id == R.id.nav_laud_seven) {
            startActivity(new Intent(MprixActivity.this, LaudSeven.class));

        } else if (id == R.id.nav_total_laoseis) {
            startActivity(new Intent(MprixActivity.this, TotalLaoseis.class));

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
