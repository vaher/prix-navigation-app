package com.aspenproductions.navigation.Arvutused;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.aspenproductions.navigation.R;

public class RabbitScreen extends Activity {

    TextView resultrprix, resultrsprix;

    int result_rprix, result_rsprix;
    double rpik, rlai, rkorg, rkad, rspik, rslai, rskorg, rskad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.rabbit_screen);

        Intent activityThatCalled = getIntent();

        double sentLauaPikkus = activityThatCalled.getDoubleExtra("lauapikkus", 1);
        double sentLauaLaius = activityThatCalled.getDoubleExtra("laualaius", 1);
        double sentLauaKorgus = activityThatCalled.getDoubleExtra("lauakorgus", 1);

        calculatePrix(sentLauaPikkus, sentLauaLaius, sentLauaKorgus);
    }
    private void calculatePrix(double sentLauaPikkus, double sentLauaLaius, double sentLauaKorgus) {
        resultrprix = (TextView)findViewById(R.id.resultrprix);
        resultrsprix = (TextView)findViewById(R.id.resultrsprix);

        SharedPreferences rprixDetails = this.getSharedPreferences("rprixdetails", MODE_PRIVATE);
        rpik = Double.parseDouble(rprixDetails.getString("rpikkus_value",""));
        rlai = Double.parseDouble(rprixDetails.getString("rlaius_value",""));
        rkorg = Double.parseDouble(rprixDetails.getString("rkorgus_value",""));
        rkad = Double.parseDouble(rprixDetails.getString("rkadu_value",""));

        SharedPreferences rsprixDetails = this.getSharedPreferences("rsprixdetails", MODE_PRIVATE);
        rspik = Double.parseDouble(rsprixDetails.getString("rspikkus_value",""));
        rslai = Double.parseDouble(rsprixDetails.getString("rslaius_value",""));
        rskorg = Double.parseDouble(rsprixDetails.getString("rskorgus_value",""));
        rskad = Double.parseDouble(rsprixDetails.getString("rskadu_value",""));

        result_rprix = (int) (((sentLauaPikkus * sentLauaLaius * sentLauaKorgus) / (rpik * rlai * rkorg))
                        * (100-rkad)/100);
        resultrprix.setText(String.valueOf(result_rprix));

        result_rsprix = (int) (((sentLauaPikkus * sentLauaLaius * sentLauaKorgus) / (rspik * rslai * rskorg))
                        * (100-rskad)/100);
        resultrsprix.setText(String.valueOf(result_rsprix));
    }

    public void onCalculateRPrixClick(View view){
        toLauaArvutusScreen(result_rprix,"R-Prix" );
    }

    public void onCalculateRSPrixClick(View view){
        toLauaArvutusScreen(result_rsprix,"RS-Prix" );
    }

    private void toLauaArvutusScreen(int result, String name) {
        Intent calculatePrix = new Intent(this, LauaArvutusScreen.class);
        calculatePrix.putExtra("prix_laudade_arv",result );
        calculatePrix.putExtra("prix_nimi",name );
        startActivity(calculatePrix);
    }
}
