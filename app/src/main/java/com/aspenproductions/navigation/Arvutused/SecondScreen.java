package com.aspenproductions.navigation.Arvutused;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.aspenproductions.navigation.R;

public class SecondScreen extends Activity
{
    TextView resultsprix, resultmprix;

    int result_sprix, result_mprix;
    double mpik, mlai, mkorg, spik, slai, skorg,
            mkad, skad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_screen);

        Intent activityThatCalled = getIntent();
        double sentLauaPikkus = activityThatCalled.getDoubleExtra("lauapikkus", 1);
        double sentLauaLaius = activityThatCalled.getDoubleExtra("laualaius", 1);
        double sentLauaKorgus = activityThatCalled.getDoubleExtra("lauakorgus", 1);

        calculatePrix(sentLauaPikkus, sentLauaLaius, sentLauaKorgus);
    }

    private void calculatePrix(double sentLauaPikkus, double sentLauaLaius, double sentLauaKorgus) {
        resultsprix = (TextView)findViewById(R.id.resultsprix);
        resultmprix = (TextView)findViewById(R.id.resultmprix);

        SharedPreferences mprixDetails = this.getSharedPreferences("mprixdetails", MODE_PRIVATE);
        mpik = Double.parseDouble(mprixDetails.getString("mpikkus_value",""));
        mlai = Double.parseDouble(mprixDetails.getString("mlaius_value",""));
        mkorg = Double.parseDouble(mprixDetails.getString("mkorgus_value",""));
        mkad = Double.parseDouble(mprixDetails.getString("mkadu_value",""));

        SharedPreferences sprixDetails = this.getSharedPreferences("sprixdetails", MODE_PRIVATE);
        spik = Double.parseDouble(sprixDetails.getString("spikkus_value",""));
        slai = Double.parseDouble(sprixDetails.getString("slaius_value",""));
        skorg = Double.parseDouble(sprixDetails.getString("skorgus_value",""));
        skad = Double.parseDouble(sprixDetails.getString("skadu_value",""));

        result_mprix = (int) (((sentLauaPikkus * sentLauaLaius * sentLauaKorgus) / (mpik * mlai * mkorg))
                        * (100 - mkad) / 100);
        resultmprix.setText(String.valueOf(result_mprix));

        result_sprix = (int) (((sentLauaPikkus * sentLauaLaius * sentLauaKorgus) / (spik * slai * skorg))
                        * (100-skad)/100);
        resultsprix.setText(String.valueOf(result_sprix));
    }

    public void onRabbitPrixShowClick(View view){
        Intent activityThatCalled = getIntent();
        Intent rabbitPrixShow = new Intent(this, RabbitScreen.class);
        rabbitPrixShow.putExtra("lauapikkus", activityThatCalled.getDoubleExtra("lauapikkus", 1));
        rabbitPrixShow.putExtra("laualaius", activityThatCalled.getDoubleExtra("laualaius", 1));
        rabbitPrixShow.putExtra("lauakorgus", activityThatCalled.getDoubleExtra("lauakorgus", 1));
        startActivity(rabbitPrixShow);
    }

    public void onCalculateSPrixClick(View view){
        toLauaArvutusScreen(result_sprix, "S-Prix");
    }

    public void onCalculateMPrixClick(View view){
        toLauaArvutusScreen(result_mprix, "M-Prix");
    }

    private void toLauaArvutusScreen(int result, String name) {
        Intent calculatePrix = new Intent(this, LauaArvutusScreen.class);
        calculatePrix.putExtra("prix_laudade_arv",result );
        calculatePrix.putExtra("prix_nimi",name );
        startActivity(calculatePrix);
    }
}
