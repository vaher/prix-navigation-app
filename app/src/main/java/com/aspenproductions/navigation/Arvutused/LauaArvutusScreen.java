package com.aspenproductions.navigation.Arvutused;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;

import com.aspenproductions.navigation.Fragments.InfoDialogFragment;
import com.aspenproductions.navigation.Lauad.LaudFive;
import com.aspenproductions.navigation.Lauad.LaudFour;
import com.aspenproductions.navigation.Lauad.LaudOne;
import com.aspenproductions.navigation.Lauad.LaudSeven;
import com.aspenproductions.navigation.Lauad.LaudSix;
import com.aspenproductions.navigation.Lauad.LaudThree;
import com.aspenproductions.navigation.Lauad.LaudTwo;
import com.aspenproductions.navigation.MainActivity;
import com.aspenproductions.navigation.R;
import com.aspenproductions.navigation.Seadistus.MprixActivity;
import com.aspenproductions.navigation.Seadistus.RSprixActivity;
import com.aspenproductions.navigation.Seadistus.RprixActivity;
import com.aspenproductions.navigation.Seadistus.SprixActivity;
import com.aspenproductions.navigation.TotalLaoseis;
import com.google.android.material.navigation.NavigationView;

public class LauaArvutusScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    TextView resultlauad;
    EditText prixide_arv;
    Button arvutus;

    float result;
    int prixid;
    int sentPrix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lauaarvutus_screen);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tulemus");

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        Intent activityThatCalled = getIntent();
        sentPrix = activityThatCalled.getIntExtra("prix_laudade_arv",1);
        TextView callingActivityMessage = (TextView) findViewById(R.id.unittext);
        callingActivityMessage.append(activityThatCalled.getStringExtra("prix_nimi"));
        callingActivityMessage.append(String.format(" (lauast sai %d prixi)",sentPrix));


        prixide_arv = (EditText)findViewById(R.id.prixide_arv);
        resultlauad =(TextView)findViewById(R.id.resultlauad);
        arvutus = (Button)findViewById(R.id.arvuta);

        arvutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prixid = Integer.parseInt(prixide_arv.getText().toString());
                result = (prixid / (float)sentPrix);
                resultlauad.setText(String.valueOf(result));
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            DialogFragment myFragment = new InfoDialogFragment();

            myFragment.show(getSupportFragmentManager(), "theDialog");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(LauaArvutusScreen.this, MainActivity.class));
        } else if (id == R.id.nav_rprix) {
            startActivity(new Intent(LauaArvutusScreen.this, RprixActivity.class));

        } else if (id == R.id.nav_sprix) {
            startActivity(new Intent(LauaArvutusScreen.this, SprixActivity.class));

        } else if (id == R.id.nav_mprix) {
            startActivity(new Intent(LauaArvutusScreen.this, MprixActivity.class));
        } else if (id == R.id.nav_rsprix) {
            startActivity(new Intent(LauaArvutusScreen.this, RSprixActivity.class));

        } else if (id == R.id.nav_laud_one) {
            startActivity(new Intent(LauaArvutusScreen.this, LaudOne.class));

        } else if (id == R.id.nav_laud_two) {
            startActivity(new Intent(LauaArvutusScreen.this, LaudTwo.class));

        } else if (id == R.id.nav_laud_three) {
            startActivity(new Intent(LauaArvutusScreen.this, LaudThree.class));

        } else if (id == R.id.nav_laud_four) {
            startActivity(new Intent(LauaArvutusScreen.this, LaudFour.class));

        } else if (id == R.id.nav_laud_five) {
            startActivity(new Intent(LauaArvutusScreen.this, LaudFive.class));

        } else if (id == R.id.nav_laud_six) {
            startActivity(new Intent(LauaArvutusScreen.this, LaudSix.class));

        } else if (id == R.id.nav_laud_seven) {
            startActivity(new Intent(LauaArvutusScreen.this, LaudSeven.class));

        } else if (id == R.id.nav_total_laoseis) {
            startActivity(new Intent(LauaArvutusScreen.this, TotalLaoseis.class));

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
